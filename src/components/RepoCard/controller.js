import { starRepoAPI, unstarRepoAPI } from '../../services/github'

export default function useActions({owner, repo}) {

    const starRepo = () => starRepoAPI(owner, repo)

    const unstarRepo = () => unstarRepoAPI(owner, repo)

    return {starRepo, unstarRepo}
}
