import React, {useState} from 'react'
import useActions from './controller.js'
import { Star } from 'styled-icons/feather'
import { Card, Button } from '@material-ui/core'
import { Container, Title, Description, RepoCardContainer, StarContainer } from './styled.js'

export default function RepoCard({name, description, stargazerCount, owner, viewerHasStarred, onStar = () => {}, onUnstar = () => {}}) {
    const [hasStarred, setHasStarred] = useState(viewerHasStarred)
    const [starCount, setStarCount] = useState(stargazerCount)
    const {starRepo, unstarRepo} = useActions({owner: owner.login, repo: name})

    const onStarHandle = () => {
        starRepo().then(() => {
            setHasStarred(true)
            setStarCount(starCount => starCount + 1)
            onStar()
        })
    }

    const onUnstarHandle = () => {
        unstarRepo().then(() => {
            setHasStarred(false)
            setStarCount(starCount => starCount - 1)
            onUnstar()
        })
    }

    return (
        <Card style={{marginBottom: 16}}>
            <RepoCardContainer>
                <Container style={{flexDirection: 'column'}}>
                    <Title>{name}</Title>
                    <Description>{description}</Description>
                    <StarContainer>
                        <Star width={18}/>
                        <Description>{starCount}</Description>
                    </StarContainer>
                </Container>
                {!hasStarred && <Button variant='contained' color='primary' disableElevation onClick={onStarHandle}>Star</Button>}
                {hasStarred && <Button variant='outlined' color='primary' onClick={onUnstarHandle}>Unstar</Button>}
            </RepoCardContainer>
        </Card>
    )
}
