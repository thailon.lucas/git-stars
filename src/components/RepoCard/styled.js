
import styled from 'styled-components'

const Container = styled.div`
    display: flex;
    flex: 1
` 
const RepoCardContainer = styled.div`
    display: flex;
    flex: 1;
    align-items: center;
    padding: 32px
` 
const StarContainer = styled.div`
    display: flex;
    margin-top: 12px;

    svg{
        margin-right: 4px;
        stroke-width: 3px;
        color: #5253B9;
    }
` 

const Title = styled.h1`
    font-size: 20px;
    line-height: 23px;
    font-weight: normal;
    margin: 0
` 

const Description = styled.h2`
    font-weight: 300;
    font-size: 16px;
    line-height: 23px;
    margin: 0
` 

export {Container, Title, Description, RepoCardContainer, StarContainer}