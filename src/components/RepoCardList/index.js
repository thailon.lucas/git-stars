import React from 'react'
import RepoCard from '../../components/RepoCard'
import {RepoCardListContainer} from './styled.js'

export default function RepoCardList({data, onStar, onUnstar}) {
    return (
        <RepoCardListContainer>
            {data && data.map(repo => (
                <RepoCard key={repo.node.id} {...repo.node} onStar={onStar} onUnstar={onUnstar}/>
            ))}
        </RepoCardListContainer>
    )
}
