
import styled from 'styled-components'

const RepoCardListContainer = styled.div`
    display: flex;
    flex: 1;
    flex-direction: column;
    background: #FFFFFF;
    box-shadow: 4px 4px 40px rgba(0, 0, 0, 0.25);
    border-radius: 5px;
    padding: 32px
` 

export {RepoCardListContainer}