import React from 'react'
import logo from '../../assets/imgs/logo.png'
import {Search} from '@styled-icons/feather/Search'
import {Logo, HeaderContainer, SearchInput, UserAvatar} from './styled'

export default function SearchHeader({onSubmit}) {

    const onKeyPressHandle = (e) => {
        var key = e.which || e.keyCode;
        if (key == 13) onSubmit(e.target.value)
    }

    return (
        <HeaderContainer>
            <Logo src={logo}/>
            <SearchInput id="standard-basic" label="Github username" onKeyPress={onKeyPressHandle}/>
            <Search width={18} style={{marginLeft: -42}}/>
            <UserAvatar alt="Thailon Lucas" src="https://avatars.githubusercontent.com/u/24868151?u=704788977b6db0f4013ebdb42ae8d4eecd79f268&v=4" />
        </HeaderContainer>
    )
}
