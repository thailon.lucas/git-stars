
import styled from 'styled-components'
import TextField from '@material-ui/core/TextField';
import Avatar from '@material-ui/core/Avatar';

const HeaderContainer = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    padding-top: 120px
` 

const Logo = styled.img`
    height: 43px;
    width: 187px;
    object-fit: contain
`

const SearchInput = styled(TextField)`
    flex: 1;
    margin: 0px 20px
`

const UserAvatar = styled(Avatar)`
    height: 64px;
    width: 64px;
    margin-left: 20px
`

export {Logo, HeaderContainer, SearchInput, UserAvatar}