import { useState } from 'react'

export default function useToast() {
    const [toastMessage, setToastMessage] = useState('')
    const [showToast, setShowToast] = useState(false)

    return {toastMessage, setToastMessage, showToast, setShowToast}
}
