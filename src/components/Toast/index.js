import React from 'react'
import { X } from 'styled-icons/feather'
import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';

const HIDE_DURATION = 2000

export default function Toast({message, show, handleClose = () => {}}) {
    return (
        <Snackbar
            anchorOrigin={{vertical: 'bottom',horizontal: 'left'}}
            open={show}
            autoHideDuration={HIDE_DURATION}
            onClose={handleClose}
            message={message}
            action={
                <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                    <X size={18} color={'#FFF'}/>
                </IconButton>
            }
      />
    )
}
