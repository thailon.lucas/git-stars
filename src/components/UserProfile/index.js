import React from 'react'
import { Coffee, Globe, MapPin, Mail } from 'styled-icons/feather'
import { UserProfileContainer, AvatarContainer, UserAvatar, Name, Username, Text, InformationContainer } from './styled'

export default function UserProfile({style, data}) {
    const {name, login, bio, email, avatarUrl, location, websiteUrl, company} = data
    
    return (
        <UserProfileContainer style={style}>
            <AvatarContainer>
                <UserAvatar alt={name} src={avatarUrl}/>
                <Name>{name}</Name>
                <Username>{login}</Username>
            </AvatarContainer>
            <InformationContainer>
                <Text style={{flex: 1}}>{bio}</Text>
                <ul>
                    {company && <li><Coffee/><Text>{company}</Text></li>}
                    {location && <li><MapPin/><Text>{location}</Text></li>}
                    {email && <li><Mail/><Text>{email}</Text></li>}
                    {websiteUrl && <li><Globe/><Text>{websiteUrl}</Text></li>}
                </ul>
            </InformationContainer>
        </UserProfileContainer>
    )
}
