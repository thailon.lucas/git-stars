
import styled from 'styled-components'
import TextField from '@material-ui/core/TextField';
import Avatar from '@material-ui/core/Avatar';

const UserProfileContainer = styled.div`
    height: 542px;
    width: 242px;
    border-radius: 5px;
    background-color: #5253B9;
    display: flex;
    flex-direction: column;
    box-shadow: 4px 4px 40px rgba(0, 0, 0, 0.25);
    position: sticky;
    z-index: -1
` 

const AvatarContainer = styled.div`
    height: 272px;
    background-color: rgba(0,0,0,0.2);
    display: flex;
    align-items:  center;
    justify-content: center;
    flex-direction: column
`

const SearchInput = styled(TextField)`
    flex: 1;
    margin: 0px 20px
`

const UserAvatar = styled(Avatar)`
    height: 160px;
    width: 160px;
`

const Name = styled.h1`
    font-size: 20px;
    color: #FFF;
    line-height: 23px;
    margin: 0;
    padding-top: 18px
`

const Username = styled.h2`
    font-weight: 300;
    font-size: 20px;
    color: #FFF;
    line-height: 23px;
    margin: 0
` 

const Text = styled.p`
    font-size: 14px;
    color: #FFF;
    line-height: 23px;
    margin: 0;
` 

const InformationContainer = styled.div`
    display: flex; 
    flex: 1;
    flex-direction: column;
    padding: 32px 16px 16px 16px;

    ul{
        margin-top: 16px 0px 0px 0px;
        padding: 0;
        word-break: break-all
    }

    li{
        display: flex;
        margin: 0;
        padding: 4px 0px;
    }

    svg{
        min-width: 18px;
        max-width: 18px;
        color: #B1B1E6;
        margin-right: 8px;
        stroke-width: 3px
    }
`

export {AvatarContainer, UserProfileContainer, SearchInput, UserAvatar, Name, Username, Text, InformationContainer}