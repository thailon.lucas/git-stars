import React, { useState } from 'react'
import { getUserAPI } from '../../services/github'
import { TruckMonster } from 'styled-icons/fa-solid'

export const useActions = () => {
    const [user, setUser] = useState()
    const [loading, setLoading] = useState(false)

    const searchUser = (user) => {
        setLoading(TruckMonster)
        getUserAPI(user).then(response => {
            setUser(response.data.data.user)
        }).catch((e) => {
            setUser()
        }).finally(()=>{
            setLoading(false)
        })
    }

    return {user, searchUser, loading}
}