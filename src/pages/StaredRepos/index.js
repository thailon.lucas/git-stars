import React from 'react'
import { useActions } from './controller'
import Toast from '../../components/Toast'
import RepoCardList from '../../components/RepoCardList'
import Container from '@material-ui/core/Container'
import UserProfile from '../../components/UserProfile'
import SearchHeader from '../../components/SearchHeader'
import useToast from '../../components/Toast/controller'
import notFoundImage from '../../assets/imgs/notfound.png'
import {MainContainer, NotFoundImage, Loading} from './styled.js'

export default function StaredReposPage() {
    const {user, searchUser, loading} = useActions()
    const {toastMessage, setToastMessage, showToast, setShowToast} = useToast()

    const onStarRepoHandle = () => {
        setToastMessage('Successfully Starred')
        setShowToast(true)
    }

    const onUnstarRepoHandle = () => {
        setToastMessage('Successfully Unstarred')
        setShowToast(true)
    }

    return (
        <Container maxWidth="lg">
            <SearchHeader onSubmit={searchUser}/>
            {user && 
                <MainContainer>
                    <UserProfile data={user} style={{marginTop: 32, marginRight: -5, marginBottom: 32, top: 30}}/>
                    <RepoCardList onStar={onStarRepoHandle} onUnstar={onUnstarRepoHandle} data={user.starredRepositories ? user.starredRepositories.edges : []}/>
                </MainContainer>
            }
            {!user && 
                <MainContainer>
                    <NotFoundImage src={notFoundImage}/>
                </MainContainer>
            }
            {loading && <Loading/>}
            <Toast message={toastMessage} show={showToast} handleClose={() => setShowToast(false)}/>
        </Container>
    )
}
