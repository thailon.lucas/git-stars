
import styled from 'styled-components'
import LinearProgress from '@material-ui/core/LinearProgress';

const MainContainer = styled.div`
    display: flex;
    padding: 60px 0px;
    background-color: red,
    align-items: center;
    justify-content: center
`

const NotFoundImage = styled.img`
    height: 600px;
    width: 300px;
    object-fit: contain;
`

const Loading = styled(LinearProgress)`
    position: fixed;
    top: 0;
    left: 0;
    width: 100vw
`


export { MainContainer, NotFoundImage, Loading }