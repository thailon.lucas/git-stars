export const API_URL = "https://api.github.com"
export const API_URL_GRAPHQL = `${API_URL}/graphql`


export const QUERIES = {
    getCurrentUser: () => `{
        viewer: { 
            login,
            avatarUrl,
            name
          }
    }`,
    getUser: (user) => `{ 
        user(login: \"${user}\") {
            id,
            login,
            name,
            avatarUrl,
            bio,
            email,
            company,
            url,
            location,
            websiteUrl,
            starredRepositories {
            edges {
                node {
                id,
                name,
                description,
                stargazerCount,
                viewerHasStarred,
                owner {
                    login
                  }
                }
            }
            }
        }
    }`
}