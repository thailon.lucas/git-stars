import axios from 'axios'
import { API_URL, API_URL_GRAPHQL, QUERIES } from '../constants'

let config = {
    headers: {
      "Content-Type": "application/json",
      Authorization: `bearer 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`
    }
  }
  
export const getCurrentUserAPI = (user) => axios.post(API_URL_GRAPHQL, {query: QUERIES.getCurrentUser()}, config)

export const getUserAPI = (user) => axios.post(API_URL_GRAPHQL, {query: QUERIES.getUser(user)}, config)

export const starRepoAPI = (owner, repo) => axios.put(`${API_URL}/user/starred/${owner}/${repo}`, {}, config)

export const unstarRepoAPI = (owner, repo) => axios.delete(`${API_URL}/user/starred/${owner}/${repo}`, config)